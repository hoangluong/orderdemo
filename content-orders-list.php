<nav aria-label="breadcrumb">
    <ol class="breadcrumb">

        <li class="breadcrumb-item active" aria-current="page">Bảng điều khiển</li>
    </ol>
</nav>

<h1>DANH SÁCH YÊU CẦU</h1>

<div class="table-border">
    <table class="table table-bordered">
        <tr>
            <th></th>
            <th class="center">Tiêu đề</th>
            <th class="center">Ngày tạo</th>
            <th class="center">Trạng thái</th>
            <th class="center">Người nhận</th>
            <th class="center"></th>
        </tr>
        <tr>
            <td></td>
            <td><a href="index.php?act=content-order-detail">TTC World - Thung lũng tình yêu: Mùa hò hẹn</a></td>
            <td>20/01/2019</td>
            <td>Đang biên tập</td>
            <td>Tungnv</td>
            <td><a href="index.php?act=content-order-edit">Sửa</a></td>
        </tr>
        <tr>
            <td></td>
            <td>Lựa chọn thông minh để du lịch nước ngoài mỗi năm ít nhất một lần</td>
            <td>11/01/2019</td>
            <td>Chờ xuất bản</td>
            <td>Tungnv</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>Những dịch vụ nổi bật trên website ngân hàng số VIB</td>
            <td>11/01/2019</td>
            <td>Hoàn thành</td>
            <td>Tungnv</td>
            <td></td>
        </tr>
    </table>
</div>

<hr>
<div class="clearfix">
    <div class="col-md-3">
        <h2 class="title-block">PR Network</h2>
    </div>
    <div class="col-md-9">
        <div style="margin-top: 10px;"></div>
        <select name="" id="" class="select2" style="margin-top: 10px;">
            <option value="" selected>Chọn lĩnh vực</option>
            <option value="">Dược phẩm - Sức khỏe</option>
            <option value="">Du lịch</option>
            <option value="">Giáo dục - Du học</option>
            <option value="">Game</option>
            <option value="">Ẩm thực</option>
            <option value="">Công nghệ</option>
            <option value="">Ô tô - Xe máy</option>
            <option value="">Thời trang - làm đẹp</option>
            <option value="">Bất động sản</option>
            <option value="">Thể thao</option>
            <option value="">Mẹ và Bé</option>
            <option value="">Đồ gia dụng</option>
        </select>

    </div>
</div>


<div class="list-channel">
    <div class="channel-item" data-toggle="modal" data-target="#myModal">
        &nbsp;<span>Kênh 14</span>
    </div>
    <div class="channel-item">
        <span>Afamily</span>
    </div>
    <div class="channel-item">
        <span>Dân trí</span>
    </div>
    <div class="channel-item">
        <span>CafeF</span>
    </div>
    <div class="channel-item">
        <span>CafeBiz</span>
    </div>
    <div class="channel-item">
        <span>Genk</span>
    </div>
    <div class="channel-item">
        <span>Gamek</span>
    </div>
    <div class="channel-item">
        <span>Vneconomy</span>
    </div>
    <div class="channel-item">
        <span>Autopro</span>
    </div>
    <div class="channel-item">
        <span>Tuổi trẻ</span>
    </div>
    <div class="channel-item">
        <span>Người lao động</span>
    </div>
    <div class="channel-item">
        <span>Kênh 14</span>
    </div>
</div>
<hr>
<h2 class="title-block">Các định dạng bài PR</h2>
<div class="list-format">
    <div class="format-item" data-toggle="modal" data-target="#dinhdangbai">
        &nbsp;<span>Bài thông thường</span>
    </div>
    <div class="format-item">
        <span>Photo Essay</span>
    </div>
    <div class="format-item">
        <span>Inforgraphic</span>
    </div>
    <div class="format-item">
        <span>Emagazine</span>
    </div>
    <div class="format-item">
        <span>MiniMagazine</span>
    </div>
</div>
<div class="col-md-12">
    <h2 class="title-block">CÁC DỊCH VỤ ĐI KÈM BÀI PR</h2>
    <div class="list-format">
        <div class="format-item"  data-toggle="modal" data-target="#chatbot">
            &nbsp;<span>Chatbot</span>
        </div>
        <div class="format-item" data-toggle="modal" data-target="#viewplus">
            <span>Viewplus</span>
        </div>
        <div class="format-item">
            <span>Adpage</span>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $('.select2').select2();
    });
</script>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Kênh 14</h4>
            </div>
            <div class="modal-body">
                Trang tin tức giải trí - xã hội Việt Nam - Quốc Tế. Đưa tin nhanh nhất : thời trang, video ngôi sao,
                phim ảnh, tình yêu, học đường, các chuyển động xã hội.
                <br>
                <br>
                <label for="">Xem báo giá: </label> <a
                        href="https://in.admicro.vn/uploads/bien-tap-thong-tin-847/afamily_admicro_prs_jul2018.pdf">tại
                    đây</a>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>

<div id="dinhdangbai" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Bài thông thường</h4>
            </div>
            <div class="modal-body">

                <br>
                <br>
                <label for="">Xem demo: </label> <a
                        href="https://in.admicro.vn/uploads/bien-tap-thong-tin-847/afamily_admicro_prs_jul2018.pdf">tại
                    đây</a>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>

<div id="chatbot" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Chatbot</h4>
            </div>
            <div class="modal-body">
               <b style="text-transform: uppercase"> BizFly Chat là gì?</b>
                <br>
<p>
                BizFly Chat là một ứng dụng trả lời tin nhắn tự động trên Website, Facebook Fanpage hay cả bài PR. Nó
                hoạt động độc lập theo kịch bản đã thiết lập để tự động trả lời những câu hỏi hoặc xử lý tình huống
                trong phạm vi cho phép.
</p>
                <br><p>
                BizFly Chat được tích hợp những tính năng nổi trội như: Tư vấn khách hàng 24/7 theo kịch bản có sẵn, lưu
                trữ - phân loại khách hàng, tích hợp đa kênh - quản lý tập trung, re-marketing với chi phí 0đ...</p>
                <label for="">Thông tin chi tiết: </label> <a
                        href="https://zchat.zamba.vn/" target="_blank">xem tại
                    đây</a>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>

<div id="viewplus" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Viewplus</h4>
            </div>
            <div class="modal-body">
                <b  style="text-transform: uppercase"> Công nghệ quảng cáo vượt trội </b><br>
                <br>
                <p>
                    <b>Targeting đa dạng </b>Target theo địa điểm (chi tiết tới từng tỉnh thành), target theo giới tính, độ tuổi, Target theo sở thích, target theo website</p>
                <br>
                <p><b>Quảng cáo đa nền tảng</b>Chỉ cần khởi tạo 1 lần duy nhất, hệ thống Viewplus sẽ tự sinh ra các format phù hợp cho cả Mobile, Tablet và PC</p>
                <br>
                <p><b>Tối ưu hóa ngân sách</b>Chủ động phân phối - điều chỉnh ngân sách phù hợp với chiến dịch, tính phí khi độc giả click vào vùng Viewplus và bài viết được load đủ nội dung</p>
                <br>
                <label for="">Thông tin chi tiết: </label> <a
                        href="https://viewplus.vn/" target="_blank">xem tại
                    đây</a>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>