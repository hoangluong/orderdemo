<nav aria-label="breadcrumb">
    <ol class="breadcrumb">

        <li class="breadcrumb-item active" aria-current="page">Danh sách</li>
    </ol>
</nav>
<h1>Thông tin chi tiết</h1>
<div class="order-detail">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Thông tin khách hàng</div>
            <div class="panel-body">
                <ul>
                    <li><label for="">Khách hàng</label> : ...</li>
                    <li><label for="">Nhãn hàng</label> : ...</li>
                    <li><label for="">......</label> : ......</li>
                </ul>
            </div>

        </div>
    </div>
    <div class="col-md-12">
        <h2><span>Nội dung</span></h2>

        <div>
            --------------------------------------------------------------------------------<br>
            --------------------------------------------------<br>
            ----------------------------------<br>
        </div>

    </div>
    <div class="clearfix">
        <div class="col-md-6">
            <h2><span>Tư vấn</span></h2>

            <div>
                <textarea name="" id="" cols="30" rows="6" class="form-control"></textarea>
            </div>

        </div>
        <div class="col-md-6">
            <h2><span>Nhân sự hỗ trợ</span></h2>
            <label for="">Pháp chế:</label>

            <div>
                <span class="label label-primary" style="font-size: 100%;">phapche1</span>
                <span class="label label-primary" style="font-size: 100%;">phapche2</span>
                <a href=""  data-toggle="modal" data-target="#phapche"><i class="glyphicon glyphicon-plus-sign"></i></a>

            </div>
            <br>
            <label for="">Copy writer:</label>

            <div>
                <span class="label label-primary" style="font-size: 100%;">copywriter1</span>
                <a href=""  data-toggle="modal" data-target="#copywrite"><i class="glyphicon glyphicon-plus-sign"></i></a>

            </div>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-md-6">
            <h2><span>Website</span></h2>

            <div><select name="" id="" class="form-control">
                    <option value="">Kênh 14</option>
                </select></div>

        </div>
        <div class="col-md-6">
            <h2><span>Định dạnh </span></h2>

            <div>
                <label class="checkbox-inline">
                    <input type="checkbox" value="">Bài thông thường
                </label>
                <br>
                <label class="checkbox-inline">
                    <input type="checkbox" value="">Emagazine
                </label>
                <br>
                <label class="checkbox-inline">
                    <input type="checkbox" value="">Quizz
                </label>
            </div>

        </div>
    </div>
    <div class="clearfix">
        <input type="submit" class="btn btn-info" value="Gửi tư vấn">

    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
</div>

<div class="modal" tabindex="-1" role="dialog" id="phapche">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="font-size: 1.25rem;" class="modal-title">Danh sách pháp chế</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -22px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                       Nguyễn Văn A - phapche1
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="" checked>
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                        Nguyễn Văn B - phapche2
                    </label>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="copywrite">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 style="font-size: 1.25rem;" class="modal-title">Lựa chọn copywrite</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -22px;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="">
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                        Nguyễn Văn A - phapche1
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="" checked>
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                        Nguyễn Văn B - phapche2
                    </label>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style>
    .checkbox label:after,
    .radio label:after {
        content: '';
        display: table;
        clear: both;
    }

    .checkbox .cr,
    .radio .cr {
        position: relative;
        display: inline-block;
        border: 1px solid #a9a9a9;
        border-radius: .25em;
        width: 1.3em;
        height: 1.3em;
        float: left;
        margin-right: .5em;
    }

    .radio .cr {
        border-radius: 50%;
    }

    .checkbox .cr .cr-icon,
    .radio .cr .cr-icon {
        position: absolute;
        font-size: .8em;
        line-height: 0;
        top: 50%;
        left: 20%;
    }

    .radio .cr .cr-icon {
        margin-left: 0.04em;
    }

    .checkbox label input[type="checkbox"],
    .radio label input[type="radio"] {
        display: none;
    }

    .checkbox label input[type="checkbox"] + .cr > .cr-icon,
    .radio label input[type="radio"] + .cr > .cr-icon {
        transform: scale(3) rotateZ(-20deg);
        opacity: 0;
        transition: all .3s ease-in;
    }

    .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
    .radio label input[type="radio"]:checked + .cr > .cr-icon {
        transform: scale(1) rotateZ(0deg);
        opacity: 1;
    }

    .checkbox label input[type="checkbox"]:disabled + .cr,
    .radio label input[type="radio"]:disabled + .cr {
        opacity: .5;
    }
</style>