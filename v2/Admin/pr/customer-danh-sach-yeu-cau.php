<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">


        <h4 class="page-title">Danh sách yêu cầu</h4>
        <p class="text-muted page-title-alt">Yêu cầu bài PR</p>

    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="button-list" style="text-align: right;">
            <button class="btn btn-primary waves-effect waves-light btn-lg">Order tư vấn đăng bài PR</button>
            <button class="btn btn-primary waves-effect waves-light btn-lg">Order sản xuất và đăng bài PR</button>

        </div>
        <hr>
        <div class="card-box">
            <h4 class="text-dark header-title m-t-0">Tìm kiếm</h4>

        </div>

        </div>
    </div>
</div>


    <div class="col-lg-12">

        <div class="portlet"><!-- /primary heading -->
            <div class="portlet-heading">
                <h3 class="portlet-title text-dark text-uppercase">
                    DANH SÁCH ORDERS
                </h3>
                <div class="portlet-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion1" href="#portlet2"><i class="ion-minus-round"></i></a>
                    <span class="divider"></span>
                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="portlet2" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover mails m-0 table table-actions-bar">
                            <thead>
                            <tr>
                                <th style="min-width: 95px;">
                                    <div class="checkbox checkbox-primary checkbox-single m-r-15">
                                        <input id="action-checkbox" type="checkbox">
                                        <label for="action-checkbox"></label>
                                    </div>

                                </th>
                                <th>Tiêu đề</th>
                                <th>Ngày tạo</th>
                                <th>Trạng thái</th>
                                <th>Nhân viên tư vấn</th>
                                <th style="min-width: 90px;">Thao tác</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr class="active">
                                <td>
                                    <div class="checkbox checkbox-primary m-r-15">
                                        <input id="checkbox2" type="checkbox" checked="">
                                        <label for="checkbox2"></label>
                                    </div>
 
                                </td>

                                <td>
                                    Lựa chọn thông minh để du lịch nước ngoài mỗi năm ít nhất một lần
                                </td>

                                <td>
                                    <a href="#">11/03/2019</a>
                                </td>

                                <td>
                                    <b><span  class="label label-success">Hoàn thành</span> </b>
                                </td>

                                <td>
                                  Trần văn A
                                </td>
                                <td>
                                    <a href="#" class="table-action-btn"><i class="md md-edit"></i></a>
                                    <a href="#" class="table-action-btn"><i class="md md-close"></i></a>
                                </td>
                            </tr>




                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div> <!-- end col -->


</div>


