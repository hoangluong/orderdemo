<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">

        <li class="breadcrumb-item active" aria-current="page">Bảng điều khiển</li>
    </ol>
</nav>

<h1>DANH SÁCH YÊU CẦU</h1>
<hr>
<div class="col-md-6">
    <input class="form-control" id="myInput" type="text" placeholder="Lọc theo từ khóa, khách hàng, Người nhận ...">
</div>
<div class="col-md-4">
    <input type="text" class="form-control daterangerpicker">
</div>
<div class="col-md-2">
    <button class="btn btn-primary">Tìm kiếm</button>
</div>
<br>
<br>
<br>

<div class="table-border">

    <table class="table table-bordered table-striped">
        <tr>

            <th class="center">Tiêu đề</th>
            <th class="center">Trạng thái</th>
            <th class="center">Thời hạn</th>
            <th class="center">Khách hàng</th>

        </tr>
        <tbody id="myTable">
        <tr>
            <td><a href="copywriter.php?act=copywriter-order-write">TTC World - Thung lũng tình yêu: Mùa hò hẹn</a></td>
            <td></td>
            <td>28/02/2019</td>
            <td>Nam Á</td>

        </tr>
        <tr>
            <td>Lựa chọn thông minh để du lịch nước ngoài mỗi năm ít nhất một lần</td>
            <td></td>
            <td>28/02/2019</td>
            <td>Vinaphone</td>
        </tr>
        <tr>

            <td>Những dịch vụ nổi bật trên website ngân hàng số VIB</td>
            <td>Hoàn thành</td>
            <td>28/02/2019</td>
            <td>VIB</td>


        </tr>
        </tbody>
    </table>
</div>

<hr>
<script>
    $(document).ready(function () {
        $("#myInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        $('.daterangerpicker').daterangepicker();

    });
</script>
