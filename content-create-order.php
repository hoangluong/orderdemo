<nav aria-label="breadcrumb">
    <ol class="breadcrumb">

        <li class="breadcrumb-item active" aria-current="page">Bảng điều khiển</li>
    </ol>
</nav>

<h2 class="title-block">đặt hàng sản xuất PR</h2>


<div class="col-md-12">
    <div class="row">

        <div class="clearfix" style="margin: 20px 0">

            <div class="col-md-12">
                <div class="row">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="clearfix">

                                <label for="" class="pad10">Tiêu đề</label>

                                <input type="text" class="form-control">

                            </div>
                            <label class="clearfix pad10" for="" style="position: relative;width: 100%;">
                                Mô tả bài viết <a href="" class="btn btn-info"
                                                  style="position: absolute;right: 0;top: 5px;" data-toggle="modal"
                                                  data-target="#myModal">Chèn bảng câu hỏi</a>
                            </label>
                            <textarea name="" class="form-control" id="motabaiviet" cols="30" rows="10">

                                </textarea>

                            <br>

                            <div class="clearfix">
                                <div class="col-md-3">
                                    Upload file
                                </div>
                                <div class="col-md-7">
                                    <input type="file" class="form-control">
                                </div>

                            </div>
                            <br>
                            <div class="clearfix">
                                <div class="col-md-3">
                                    Upload bài viết
                                </div>
                                <div class="col-md-7">
                                    <input type="file" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <div class="row"><a data-toggle="modal"
                                                        data-target="#soanthaobaiviet" class="btn btn-primary editor-btn">Soạn
                                            thảo bài viết</a></div>
                                </div>
                            </div>
                            <div class="clearfix editor" style="display: none">
                                <br>
                                <label for="">Nội dung bài viết</label>
                                <textarea name="" class="form-control ckeditor" id="editor1" cols="30" rows="10">

                                </textarea>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3">
                                    <br>
                                    Chèn tags
                                </div>

                                <div class="col-md-9">
                                    <br>
                                    <input type="text" class="form-control">
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-12">
                <div class="center">

                    <input type="submit" class="btn btn-primary" value="Gửi yêu cầu">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 80%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Bảng câu hỏi gợi ý</h4>
            </div>
            <div class="modal-body">
                <textarea name="" id="editor" cols="30" rows="10">
                  <table width="790">
<tbody>
<tr>
<td colspan="2" width="790" style="background: yellow">I/ THÔNG TIN KHÁCH HÀNG</td>
</tr>
<tr>
<td width="271">Nhãn</td>
<td width="519" style="width: 519px !important;"></td>
</tr>
<tr>
<td width="271">Website</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Landing Page</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Fanpage</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Instagram (nếu có)</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Đầu mối phụ trách truyền thông
(Liên hệ khi cần thiết)</td>
<td width="519"></td>
</tr>
<tr>
<td colspan="2" width="790" style="background: yellow">II/ NỘI DUNG ORDER</td>
</tr>
<tr>
<td width="271">Nội dung bài viết mong muốn</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Sản phẩm/Dịch vụ/Thông điệp truyền thông muốn đề cập đến trong bài viết</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Bài viết thuộc chiến dịch truyền thông nào của khách hàng</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Mục đích viết bài
(Mục tiêu truyền thông)</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Kênh đăng tải</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Vị trí đăng tải mong muốn</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Chân dung độc giả nhắm đến
+ Vùng miền
+ Độ tuổi
+ Giới tính
+ Mức thu nhập/Mức sống</td>
<td width="519"></td>
</tr>
<tr>
<td colspan="2" width="790" style="background: yellow">III/ YÊU CẦU THÔNG TIN CẦN CUNG CẤP ĐỂ TƯ VẤN ANGLE</td>
</tr>
<tr>
<td width="271">Thông tin chi tiết sản phẩm/dịch vụ/Doanh Nghiệp</td>
<td></td>
</tr>
<tr>
<td width="271">Các USPs muốn làm nổi bật trong bài viết</td>
<td></td>
</tr>
<tr>
<td width="271">Điểm khác biệt/lợi thế so với đổi thủ</td>
<td></td>
</tr>
<tr>
<td width="271">Điểm yếu so với đối thủ</td>
<td></td>
</tr>
<tr>
<td colspan="2" width="790" style="background: yellow">IV/ YÊU CẦU THÊM TƯ LIỆU SẢN XUẤT BÀI VIẾT</td>
</tr>
<tr>
<td width="271">Các thông tin sâu về sản phẩm
Số liệu phân tích thị trường</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Câu trả lời với một số câu hỏi phỏng vấn từ Phóng viên trong trường hợp đặc biệt</td>
<td width="519"></td>
</tr>
<tr>
<td width="271">Hình ảnh
Keyvisual của khách hàng</td>
<td></td>
</tr>
</tbody>
</table>

                </textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="chen">Chèn</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>
<div id="soanthaobaiviet" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 80%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Soạn thảo bài viết</h4>
            </div>
            <div class="modal-body">
                <textarea name="" id="editor2" cols="30" rows="10"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Lưu</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>

    </div>
</div>
<script>
    CKEDITOR.replace('editor', {
        height: 500
    });
    CKEDITOR.replace('editor1', {
        height: 500
    });
    CKEDITOR.replace('editor2', {
        height: 500
    });
    $(document).ready(function () {
        $('#chen').click(function () {
            $('#motabaiviet').val($('#editor').val());
            CKEDITOR.replace('motabaiviet', {
                height: 500
            });
            $('#myModal').modal('toggle');
        })
    })
</script>
<script>
    $('.editor-btn').click(function () {
        $('.editor').toggle('slideup');
    });

</script>
<style>
    .multiselect-native-select {
        position: relative;

    select {
        border: 0 !important;
        clip: rect(0 0 0 0) !important;
        height: 1px !important;
        margin: -1px -1px -1px -3px !important;
        overflow: hidden !important;
        padding: 0 !important;
        position: absolute !important;
        width: 1px !important;

        top: 30px;
    }

    }
    .multiselect-container {
        position: absolute;
        list-style-type: none;
        margin: 0;
        padding: 0;

    .input-group {
        margin: 5px;
    }

    li {
        padding: 0;

    .multiselect-all {

    label {
        font-weight: 700;
    }

    }
    a {
        padding: 0;

    label {
        margin: 0;
        height: 100%;
        cursor: pointer;
        font-weight: 400;
        padding: 3px 20px 3px 40px;

    input[type=checkbox] {
        margin-bottom: 5px;
    }

    }
    label.radio {
        margin: 0;
    }

    label.checkbox {
        margin: 0;
    }

    }
    }
    li.multiselect-group {

    label {
        margin: 0;
        padding: 3px 20px 3px 20px;
        height: 100%;
        font-weight: 700;
    }

    }
    li.multiselect-group-clickable {

    label {
        cursor: pointer;
    }

    }
    }
    .btn-group {

    .btn-group {

    .multiselect.btn {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    }
    }
    .form-inline {

    .multiselect-container {

    label.checkbox {
        padding: 3px 20px 3px 40px;
    }

    label.radio {
        padding: 3px 20px 3px 40px;
    }

    li {

    a {

    label.checkbox {

    input[type=checkbox] {
        margin-left: -20px;
        margin-right: 0;
    }

    }
    label.radio {

    input[type=radio] {
        margin-left: -20px;
        margin-right: 0;
    }

    }
    }
    }
    }
    }

    }

    .container {
        max-width: 1170px;
        margin: auto;
    }

    img {
        max-width: 100%;
    }

    .inbox_people {
        background: #f8f8f8 none repeat scroll 0 0;
        float: left;
        overflow: hidden;
        width: 40%;
        border-right: 1px solid #c4c4c4;
    }

    .inbox_msg {
        border: 1px solid #c4c4c4;
        clear: both;
        overflow: hidden;
    }

    .top_spac {
        margin: 20px 0 0;
    }

    .recent_heading {
        float: left;
        width: 40%;
    }

    .srch_bar {
        display: inline-block;
        text-align: right;
        width: 60%;
        padding:
    }

    .headind_srch {
        padding: 10px 29px 10px 20px;
        overflow: hidden;
        border-bottom: 1px solid #c4c4c4;
    }

    .recent_heading h4 {
        color: #05728f;
        font-size: 21px;
        margin: auto;
    }

    .srch_bar input {
        border: 1px solid #cdcdcd;
        border-width: 0 0 1px 0;
        width: 80%;
        padding: 2px 0 4px 6px;
        background: none;
    }

    .srch_bar .input-group-addon button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        padding: 0;
        color: #707070;
        font-size: 18px;
    }

    .srch_bar .input-group-addon {
        margin: 0 0 0 -27px;
    }

    .chat_ib h5 {
        font-size: 15px;
        color: #464646;
        margin: 0 0 8px 0;
    }

    .chat_ib h5 span {
        font-size: 13px;
        float: right;
    }

    .chat_ib p {
        font-size: 14px;
        color: #989898;
        margin: auto
    }

    .chat_img {
        float: left;
        width: 11%;
    }

    .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
    }

    .chat_people {
        overflow: hidden;
        clear: both;
    }

    .chat_list {
        border-bottom: 1px solid #c4c4c4;
        margin: 0;
        padding: 18px 16px 10px;
    }

    .inbox_chat {
        height: 550px;
        overflow-y: scroll;
    }

    .active_chat {
        background: #ebebeb;
    }

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }

    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
    }

    .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }

    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }

    .received_withd_msg {
        width: 57%;
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0;
        color: #fff;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }

    .outgoing_msg {
        overflow: hidden;
        margin: 26px 0 26px;
    }

    .sent_msg {
        float: right;
        width: 46%;
    }

    .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
    }

    .type_msg {
        border-top: 1px solid #c4c4c4;
        position: relative;
    }

    .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border: medium none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        height: 33px;
        position: absolute;
        right: 0;
        top: 11px;
        width: 33px;
    }

    .messaging {
        padding: 0 0 50px 0;
    }

    .msg_history {
        height: 300px;
        overflow-y: auto;
    }

    .open > .dropdown-menu {
        display: block;
        width: 100%;
    }
</style>
<script>
    $(document).ready(function () {
        $('#multiselect').multiselect({
            buttonWidth: '100%',
            includeSelectAllOption: true,
            nonSelectedText: 'Chọn kênh'
        });
    });

    function getSelectedValues() {
        var selectedVal = $("#multiselect").val();
        for (var i = 0; i < selectedVal.length; i++) {
            function innerFunc(i) {
                setTimeout(function () {
                    location.href = selectedVal[i];
                }, i * 2000);
            }

            innerFunc(i);
        }
    }


</script>