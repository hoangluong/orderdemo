<nav aria-label="breadcrumb">
    <ol class="breadcrumb">

        <li class="breadcrumb-item active" aria-current="page">Bảng điều khiển</li>
    </ol>
</nav>

<h2 class="title-block">Sửa thông tin</h2>


<div class="col-md-12">
    <div class="row">

        <div class="clearfix" style="margin: 20px 0">

            <div class="col-md-12">
                <div class="row">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="clearfix">

                                <label for="" class="pad10">Tiêu đề</label>

                                <input type="text" class="form-control"
                                       value="TTC World - Thung lũng tình yêu: Mùa hò hẹn">

                            </div>
                            <label class="clearfix pad10" for="">
                                Mô tả bài viết
                            </label>
                                <textarea name="" class="form-control" id="" cols="30" rows="10">

                                </textarea>

                            <br>

                            <div class="clearfix">
                                <div class="col-md-3">
                                    Upload bài viết
                                </div>
                                <div class="col-md-7">
                                    <input type="file" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <div class="row"><a href="javascript:void(0);" class="btn btn-primary editor-btn">Soạn
                                            thảo bài viết</a></div>
                                </div>
                            </div>
                            <div class="clearfix editor">
                                <br>
                                <label for="">Nội dung bài viết</label>
                                 <textarea name="content" class="form-control ckeditor" id="editor1" cols="30" rows="100">
                                     <div class="klw-body-top">


                                         <div class="klw-new-content">


                                             <h2 class="knc-sapo">
                                                 Đà Lạt - "thành phố tình yêu" mộng mơ và lãng mạn, luôn hấp dẫn các cặp
                                                 đôi mùa hò hẹn. Phố núi đang ngập tràn sắc xuân hòa cùng giai điệu
                                                 những bản tình ca ngọt ngào của mùa Valentine - lễ tình nhân.
                                             </h2>


                                             <div class="react-relate animated hiding-react-relate">
                                             </div>

                                             <div data-check-position="body_start"></div>
                                             <div class="knc-content">


                                                 <!-- Kham pha -->


                                                 <p><b style="font-size: 13pt;">Đưa em về thanh xuân</b><br></p>

                                                 <p>Nằm trong chuỗi sự kiện Valentine nổi tiếng gắn liền với thương hiệu
                                                     TTC World - Thung Lũng Tình Yêu (trực thuộc ngành Du lịch TTC - TTC
                                                     Hospitality). Chương trình năm nay diễn ra với chủ đề "Đưa em về
                                                     thanh xuân". Toàn bộ khu du lịch được biến hóa thành chuyến tàu đưa
                                                     du khách đi về thời thanh xuân của những năm 80 với hàng loạt các
                                                     tiểu cảnh, khu vực trang trí mang màu sắc cổ điển.</p>

                                                 <div class="VCSortableInPreviewMode" style="" type="Photo">
                                                     <div>
                                                         <a href="http://channel.mediacdn.vn/2019/2/18/untitled-1550480180708235950270.png"
                                                            data-fancybox-group="img-lightbox"
                                                            title="Các cặp tình nhân cùng dạo bước trên những cung đường đầy lãng mạn của TTC World - Thung lũng Tình yêu"
                                                            target="_blank" class="detail-img-lightbox"><img
                                                                 style="width:100%;max-width:100%;"
                                                                 src="http://channel.mediacdn.vn/2019/2/18/untitled-1550480180708235950270.png"
                                                                 id="img_7d45aeb0-335b-11e9-894a-7515f532d964" w="952"
                                                                 h="304"
                                                                 alt="TTC World - Thung lũng tình yêu: Mùa hò hẹn - Ảnh 1."
                                                                 title="TTC World - Thung lũng tình yêu: Mùa hò hẹn - Ảnh 1."
                                                                 rel="lightbox"
                                                                 photoid="7d45aeb0-335b-11e9-894a-7515f532d964"
                                                                 type="photo"
                                                                 data-original="http://channel.mediacdn.vn/2019/2/18/untitled-1550480180708235950270.png"
                                                                 width="" height="" class="lightbox-content"></a></div>
                                                     <div class="PhotoCMS_Caption"><p
                                                             data-placeholder="[nhập chú thích]">Các cặp tình nhân cùng
                                                             dạo bước trên những cung đường đầy lãng mạn của TTC World -
                                                             Thung lũng Tình yêu</p></div>
                                                 </div>
                                                 <p>Sự kiện đã thu hút sự quan tâm của hàng nghìn người, nhất là các bạn
                                                     trẻ và những cặp uyên ương. Tại đây, các cặp đôi được trải nghiệm
                                                     những bức thư tình thập niên cũ, được tham gia trò chơi gắn kết
                                                     tình cảm, như: Tình như ông bà anh, Thấu hiểu nhau - Yêu bền lâu,
                                                     kỷ vật tình yêu, tình ca bất hủ... và hòa mình vào những bản tình
                                                     ca ngọt ngào. Ngoài ra, khi đến thăm quan khu du lịch đúng ngày
                                                     14/02 - dịp lễ tình nhân, các cặp đôi khi mặc áo cặp chỉ phải mua 1
                                                     vé cho 2 người.</p>

                                                 <div class="VCSortableInPreviewMode" style="" type="Photo">
                                                     <div>
                                                         <a href="http://channel.mediacdn.vn/2019/2/18/photo-2-15504800969131529533912.jpg"
                                                            data-fancybox-group="img-lightbox"
                                                            title="Sự kiện triển lãm và không gian thanh xuân đã được tái hiện từ ngày 14 đến 16/02/2019, riêng chương trình âm nhạc và các trò chơi được diễn ra vào 16/02/2019"
                                                            target="_blank" class="detail-img-lightbox"><img
                                                                 style="width:100%;max-width:100%;"
                                                                 src="http://channel.mediacdn.vn/2019/2/18/photo-2-15504800969131529533912.jpg"
                                                                 id="img_4b1c30d0-335b-11e9-8304-257647080328" w="960"
                                                                 h="960"
                                                                 alt="TTC World - Thung lũng tình yêu: Mùa hò hẹn - Ảnh 2."
                                                                 title="TTC World - Thung lũng tình yêu: Mùa hò hẹn - Ảnh 2."
                                                                 rel="lightbox"
                                                                 photoid="4b1c30d0-335b-11e9-8304-257647080328"
                                                                 type="photo"
                                                                 data-original="http://channel.mediacdn.vn/2019/2/18/photo-2-15504800969131529533912.jpg"
                                                                 width="" height="" class="lightbox-content"></a></div>
                                                     <div class="PhotoCMS_Caption"><p
                                                             data-placeholder="[nhập chú thích]">Sự kiện triển lãm và
                                                             không gian thanh xuân đã được tái hiện từ ngày 14 đến
                                                             16/02/2019, riêng chương trình âm nhạc và các trò chơi được
                                                             diễn ra vào 16/02/2019</p></div>
                                                 </div>
                                                 <p><b>Triển lãm thư tình</b></p>

                                                 <p>Triển lãm thư tình là điểm nhấn không thể bỏ qua của ngày hội. Tại
                                                     đây, du khách được chứng kiến những bức thư tình viết tay mang đậm
                                                     chất tình ngày xưa với ngôn ngữ, chữ viết, màu mực, màu giấy… đều
                                                     thể hiện sự chân thành, nhẹ nhàng mà sâu sắc.</p>

                                                 <div class="VCSortableInPreviewMode" style="" type="Photo">
                                                     <div>
                                                         <a href="http://channel.mediacdn.vn/2019/2/18/untitled-15504803577231032036923.png"
                                                            data-fancybox-group="img-lightbox"
                                                            title="Sự kiện là nơi tụ hội những tình yêu đẹp nhất trong nắng mùa xuân giữa phố núi lãng mạn"
                                                            target="_blank" class="detail-img-lightbox"><img
                                                                 style="width:100%;max-width:100%;"
                                                                 src="http://channel.mediacdn.vn/2019/2/18/untitled-15504803577231032036923.png"
                                                                 id="img_e7581220-335b-11e9-97a6-4bb0d861b42b" w="1191"
                                                                 h="398"
                                                                 alt="TTC World - Thung lũng tình yêu: Mùa hò hẹn - Ảnh 3."
                                                                 title="TTC World - Thung lũng tình yêu: Mùa hò hẹn - Ảnh 3."
                                                                 rel="lightbox"
                                                                 photoid="e7581220-335b-11e9-97a6-4bb0d861b42b"
                                                                 type="photo"
                                                                 data-original="http://channel.mediacdn.vn/2019/2/18/untitled-15504803577231032036923.png"
                                                                 width="" height="" class="lightbox-content"></a></div>
                                                     <div class="PhotoCMS_Caption"><p
                                                             data-placeholder="[nhập chú thích]">Sự kiện là nơi tụ hội
                                                             những tình yêu đẹp nhất trong nắng mùa xuân giữa phố núi
                                                             lãng mạn</p></div>
                                                 </div>
                                                 <div id="admzone474524" class="wp100 fl mt-10">
                                                     <div id="ads_zone474524">
                                                         <div id="ads_zone474524_slot1">
                                                             <div id="ads_zone474524_banner553389" class="banner0">
                                                                 <div id="sspbid_3192"></div>
                                                             </div>
                                                         </div>
                                                         <div id="ads_zone474524_slot2" style="display: none;"></div>
                                                     </div>
                                                 </div>
                                                 <p><b>Chìm trong những bài tình ca bất tận</b></p>

                                                 <p>Sân khấu được bố trí trong một không gian ngập tràn sắc hoa cùng
                                                     nhiều tiểu cảnh sinh động dành cho các đôi tình nhân thưởng lãm,
                                                     chụp hình lưu niệm. Ngoài các tiết mục văn nghệ sôi động và những
                                                     trò chơi thú vị là giây phút lắng động cảm xúc của các cặp đôi tỏ
                                                     tình, cầu hôn đã để lại nhiều ấn tượng đẹp cho người tham dự.</p>

                                                 <div class="VCSortableInPreviewMode" style="" type="Photo">
                                                     <div>
                                                         <a href="http://channel.mediacdn.vn/2019/2/18/photo-5-15504800969271729798701.jpg"
                                                            data-fancybox-group="img-lightbox"
                                                            title="Cặp đôi cầu hôn đã mang đến khán giả những giây phút rất xúc động"
                                                            target="_blank" class="detail-img-lightbox"><img
                                                                 style="width:100%;max-width:100%;"
                                                                 src="http://channel.mediacdn.vn/2019/2/18/photo-5-15504800969271729798701.jpg"
                                                                 id="img_4b1f8c30-335b-11e9-894a-7515f532d964" w="1202"
                                                                 h="797"
                                                                 alt="TTC World - Thung lũng tình yêu: Mùa hò hẹn - Ảnh 4."
                                                                 title="TTC World - Thung lũng tình yêu: Mùa hò hẹn - Ảnh 4."
                                                                 rel="lightbox"
                                                                 photoid="4b1f8c30-335b-11e9-894a-7515f532d964"
                                                                 type="photo"
                                                                 data-original="http://channel.mediacdn.vn/2019/2/18/photo-5-15504800969271729798701.jpg"
                                                                 width="" height="" class="lightbox-content"></a></div>
                                                     <div class="PhotoCMS_Caption"><p
                                                             data-placeholder="[nhập chú thích]">Cặp đôi cầu hôn đã mang
                                                             đến khán giả những giây phút rất xúc động</p></div>
                                                 </div>
                                                 <p><b>Nơi tình nhân tìm đến</b></p>

                                                 <p>TTC World - Thung lũng Tình yêu được xem như Đà Lạt thu nhỏ, nơi đây
                                                     như "chốn tình nhân" đẹp nghiêng mình "đốn tim" bao khách lữ hành.
                                                     Nếu có dịp du lịch Đà Lạt, bạn sẽ bị đắm chìm trong không gian lãng
                                                     mạn của một nơi đã đi vào thơ ca, những cung đường, những bậc thang
                                                     đẹp hút hồn nhuốm màu của thời gian.</p>

                                                 <p>Ngoài các địa điểm như vườn Thượng Uyển, vườn Hồng Hạc, phố hoa, cầu
                                                     thang hoa 3D, Mê cung Tình yêu, vườn hoa cẩm tú cầu diện tích lên
                                                     đến 5.000 m2… thì hàng loạt các hạng mục đầu tư mới chắc chắn sẽ
                                                     làm du khách không thể kiềm lòng. Thung lũng chè "không góc chết"
                                                     rộng đến 8.000 m2, khu vực Tình yêu vĩnh cửu, vườn hoa hồng rộng
                                                     13.200 m2, mô hình Tạo tác nghệ thuật cây xanh với diện tích đến
                                                     3.000 m2, bảo tàng tượng sáp… là những hạng mục sắp được giới thiệu
                                                     đến du khách.</p>

                                                 <p>Với những cảnh quan mới có kiến trúc độc đáo, gần gũi thiên nhiên
                                                     hòa quyện cùng không gian yên bình nơi phố núi, TTC World - Thung
                                                     lũng Tình yêu sẽ mang đến cho du khách những trải nghiệm đẹp.</p>

                                                 <p><br></p>

                                                 <div class="VCSortableInPreviewMode" style="background-color:#FFFBF1;"
                                                      data-back="#FFFEC7" data-border="#FF7D00"
                                                      id="ObjectBoxContent_1550480492512" type="content">
                                                     <div placeholder="[nhập nội dung]"><p>TTC World - Thung lũng Tình
                                                             yêu hợp nhất 2 khu du lịch Thung lũng Tình yêu, đồi Mộng Mơ
                                                             với tổng diện tích lên đến 137 ha. Khu du lịch đã nhận được
                                                             nhiều giải thưởng, danh hiệu uy tín, như: Top 7 điểm thăm
                                                             quan du lịch hàng đầu Việt Nam 2017; Top 10 Sản phẩm - Dịch
                                                             vụ uy tín, an toàn, chất lượng 2017; Top 10 Thương hiệu
                                                             tiêu biểu hội nhập Châu Á - Thái Bình Dương 2018; Chứng
                                                             nhận đánh giá quốc tế độc lập "Doanh nghiệp xuất sắc đạt
                                                             chuẩn CSI - Hài lòng khách hàng" 2018; Chứng nhận đánh giá
                                                             quốc tế độc lập "Doanh nghiệp đạt chuẩn QSI - Dịch vụ chất
                                                             lượng cao 2018"; Chứng nhận đánh giá quốc tế độc lập
                                                             "Trusted Quality Supplier - Nhà cung cấp chất lượng 2018",
                                                             The Guide Awards 2017 - 2018 - Ốc đảo xanh hoàn hảo cho
                                                             những cặp đôi đến Đà Lạt; Điểm mua sắm chất lượng cao và
                                                             nhãn hiệu xanh…</p>

                                                         <p>Ngoài ra, TTC World - Thung lũng Tình yêu đã được nhận chứng
                                                             chỉ IAAPA về khu vui chơi đạt tiêu chuẩn quốc tế uy tín thế
                                                             giới, chính thức trở thành thành viên của hiệp hội này.</p>

                                                         <p><b>Khu du lịch TTC World - Thung lũng Tình yêu:</b></p>

                                                         <p>Địa chỉ: 03 - 05 - 07 Mai Anh Đào, phường 8, TP. Đà Lạt</p>

                                                         <p>Hotline: &lrm;&lrm;&lrm;&lrm;1900 55 88 55 - &lrm;&lrm;&lrm;&lrm;0942
                                                             157 099</p>

                                                         <p>Facebook: Thung lũng Tình yêu</p></div>
                                                 </div>


                                             </div>
                                             <!-- LIVE -->


                                             <div data-check-position="body_end"></div>

                                         </div>


                                     </div>
                                </textarea>
                            </div>
                            <div class="clearfix">
                                <div class="col-md-3">
                                    <br>
                                    Chèn tag
                                </div>

                                <div class="col-md-9">
                                    <br>
                                    <input type="text" class="form-control tags" value="Thung lũng Tình Yêu,thành phố tình yêu,Lễ tình nhân" data-role="tagsinput">
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-12">
                <div class="center">

                    <input type="submit" class="btn btn-primary" value="Cập nhật thông tin">
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    CKEDITOR.replace( 'editor1',{
        height:900
    } );
</script>
<script>

    ;
    $('.editor-btn').click(function () {
        $('.editor').toggle('slideup');
    });

</script>
<style>

    .multiselect-native-select {
        position: relative;

    select {
        border: 0 !important;
        clip: rect(0 0 0 0) !important;
        height: 1px !important;
        margin: -1px -1px -1px -3px !important;
        overflow: hidden !important;
        padding: 0 !important;
        position: absolute !important;
        width: 1px !important;

        top: 30px;
    }

    }
    .multiselect-container {
        position: absolute;
        list-style-type: none;
        margin: 0;
        padding: 0;

    .input-group {
        margin: 5px;
    }

    li {
        padding: 0;

    .multiselect-all {

    label {
        font-weight: 700;
    }

    }
    a {
        padding: 0;

    label {
        margin: 0;
        height: 100%;
        cursor: pointer;
        font-weight: 400;
        padding: 3px 20px 3px 40px;

    input[type=checkbox] {
        margin-bottom: 5px;
    }

    }
    label.radio {
        margin: 0;
    }

    label.checkbox {
        margin: 0;
    }

    }
    }
    li.multiselect-group {

    label {
        margin: 0;
        padding: 3px 20px 3px 20px;
        height: 100%;
        font-weight: 700;
    }

    }
    li.multiselect-group-clickable {

    label {
        cursor: pointer;
    }

    }
    }
    .btn-group {

    .btn-group {

    .multiselect.btn {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    }
    }
    .form-inline {

    .multiselect-container {

    label.checkbox {
        padding: 3px 20px 3px 40px;
    }

    label.radio {
        padding: 3px 20px 3px 40px;
    }

    li {

    a {

    label.checkbox {

    input[type=checkbox] {
        margin-left: -20px;
        margin-right: 0;
    }

    }
    label.radio {

    input[type=radio] {
        margin-left: -20px;
        margin-right: 0;
    }

    }
    }
    }
    }
    }

    }


</style>
<script>
    $(document).ready(function () {
        $('#multiselect').multiselect({
            buttonWidth: '100%',
            includeSelectAllOption: true,
            nonSelectedText: 'Chọn kênh'
        });
    });

    function getSelectedValues() {
        var selectedVal = $("#multiselect").val();
        for (var i = 0; i < selectedVal.length; i++) {
            function innerFunc(i) {
                setTimeout(function () {
                    location.href = selectedVal[i];
                }, i * 2000);
            }

            innerFunc(i);
        }
    }


</script>