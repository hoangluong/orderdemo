<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">

        <li class="breadcrumb-item active" aria-current="page">Bảng điều khiển</li>
    </ol>
</nav>

<h1>DANH SÁCH YÊU CẦU</h1>
<hr>
<div class="col-md-6">
    <input class="form-control" id="myInput" type="text" placeholder="Lọc theo từ khóa, khách hàng, Người nhận ...">
</div>
<div class="col-md-4">
    <input type="text" class="form-control daterangerpicker">
</div>
<div class="col-md-2">
    <button class="btn btn-primary">Tìm kiếm</button>
</div>
<br>
<br>
<br>

<div class="table-border">

    <table class="table table-bordered table-striped">
        <tr>

            <th class="center">Tiêu đề</th>
            <th class="center">Ngày tạo</th>
            <th class="center">Trạng thái</th>
            <th class="center">Khách hàng</th>
            <th class="center">Người nhận</th>
            <th class="center"></th>
        </tr>
        <tbody id="myTable">
        <tr>

            <td><a href="account.php?act=account-order-detail">TTC World - Thung lũng tình yêu: Mùa hò hẹn</a></td>
            <td>20/01/2019</td>
            <td></td>
            <td>Nam Á</td>
            <td>Tungnv</td>
            <td></td>
        </tr>
        <tr>

            <td>Lựa chọn thông minh để du lịch nước ngoài mỗi năm ít nhất một lần</td>
            <td>11/01/2019</td>
            <td></td>
            <td>Vinaphone</td>
            <td><a href=""><i class="glyphicon glyphicon-plus"></i></a></td>
            <td><a href="#">nhận yêu cầu</a></td>
        </tr>
        <tr>

            <td>Những dịch vụ nổi bật trên website ngân hàng số VIB</td>
            <td>11/01/2019</td>
            <td></td>
            <td>VIB</td>
            <td><a href=""><i class="glyphicon glyphicon-plus"></i></a></td>
            <td><a href="index.php?act=content-order-edit">nhận yêu cầu</a></td>
        </tr>
        </tbody>
    </table>
</div>

<hr>
<script>
    $(document).ready(function () {
        $("#myInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        $('.daterangerpicker').daterangepicker();

    });
</script>