<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"
          rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet">

    <link rel="stylesheet" href="demo.css">
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.2/standard-all/ckeditor.js"></script>


    <title></title>
</head>
<body>
<div class="wrapper">
    <section class="header">
        <div class="container">
            <div class="logo hidden-xs">
                <a href="https://pr.admicro.vn/" class="menu-logo">
                    <img alt="Booking PR Admicro" src="https://pr.admicro.vn/skins/images/admicro.png">
                </a>
            </div>

        </div>
    </section>

    <div class="container">
        <div class="main clearfix">

            <div class="col-md-9">
                <?php include_once(isset($_GET['act']) ? $_GET['act'] . '.php' : 'content-orders-list.php'); ?>
            </div>

            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Danh mục</div>
                    <div class="panel-body"><a href="index.php?act=content-orders-list">Danh sách yêu cầu</a></div>
                    <div class="panel-body"><a href="index.php?act=content-create-order">Đặt hàng sản xuất bài PR</a></div>
                    <div class="panel-body"><a href="index.php?act=content-create-dangbai">Tư vấn đăng bài PR</a></div>

                </div>
                <!-- start chat-->
                <?php if((isset($_GET['act'])) && in_array($_GET['act'],array('content-order-detail'))){?>
                <div class="panel panel-default">
                    <div class="panel-body" style="padding: 10px 3px;">
                        <div class="mesgs">
                            <div class="msg_history">
                                <div class="incoming_msg">
                                    <div class="incoming_msg_img"><img
                                            src="https://ptetutorials.com/images/user-profile.png" alt="sunil"
                                            data-pagespeed-url-hash="4110524464"
                                            >
                                    </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <p>Test which is a new approach to have all
                                                solutions</p>
                                            <span class="time_date"> 11:01 AM | June 9</span></div>
                                    </div>
                                </div>
                                <div class="outgoing_msg">
                                    <div class="sent_msg">
                                        <p>Test which is a new approach to have all
                                            solutions</p>
                                        <span class="time_date"> 11:01 AM | June 9</span></div>
                                </div>
                                <div class="incoming_msg">
                                    <div class="incoming_msg_img"><img
                                            src="https://ptetutorials.com/images/user-profile.png" alt="sunil"
                                            data-pagespeed-url-hash="4110524464"
                                           >
                                    </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <p>Test, which is a new approach to have</p>
                                            <span class="time_date"> 11:01 AM | Yesterday</span></div>
                                    </div>
                                </div>
                                <div class="outgoing_msg">
                                    <div class="sent_msg">
                                        <p>Apollo University, Delhi, India Test</p>
                                        <span class="time_date"> 11:01 AM | Today</span></div>
                                </div>
                                <div class="incoming_msg">
                                    <div class="incoming_msg_img"><img
                                            src="https://ptetutorials.com/images/user-profile.png" alt="sunil"
                                            data-pagespeed-url-hash="4110524464"
                                            >
                                    </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <p>We work directly with our designers and suppliers,
                                                and sell direct to you, which means quality, exclusive
                                                products, at a price anyone can afford.</p>
                                            <span class="time_date"> 11:01 AM | Today</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="type_msg">
                                <div class="input_msg_write">
                                    <input type="text" class="write_msg" placeholder="Type a message"/>
                                    <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o"
                                                                                  aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
                <!-- end chat-->
            </div>

        </div>
    </div>
    <div class="footer" style="background: #ffffff;margin: 20px 0;padding: 20px">
      <div class="container">
          <div style="text-align: center">2019</div>
      </div>
    </div>
</div>

<style>
    .ck-content {
        min-height: 300px;
    }

    .container {
        max-width: 1170px;
        margin: auto
    }

    img {
        max-width: 100%
    }

    .inbox_people {
        background: #f8f8f8 none repeat scroll 0 0;
        float: left;
        overflow: hidden;
        width: 40%;
        border-right: 1px solid #c4c4c4
    }

    .inbox_msg {
        border: 1px solid #c4c4c4;
        clear: both;
        overflow: hidden
    }

    .top_spac {
        margin: 20px 0 0
    }

    .recent_heading {
        float: left;
        width: 40%
    }

    .srch_bar {
        display: inline-block;
        text-align: right;
        width: 60%;
        padding:
    }

    .headind_srch {
        padding: 10px 29px 10px 20px;
        overflow: hidden;
        border-bottom: 1px solid #c4c4c4
    }

    .recent_heading h4 {
        color: #05728f;
        font-size: 21px;
        margin: auto
    }

    .srch_bar input {
        border: 1px solid #cdcdcd;
        border-width: 0 0 1px 0;
        width: 80%;
        padding: 2px 0 4px 6px;
        background: none
    }

    .srch_bar .input-group-addon button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        padding: 0;
        color: #707070;
        font-size: 18px
    }

    .srch_bar .input-group-addon {
        margin: 0 0 0 -27px
    }

    .chat_ib h5 {
        font-size: 15px;
        color: #464646;
        margin: 0 0 8px 0
    }

    .chat_ib h5 span {
        font-size: 13px;
        float: right
    }

    .chat_ib p {
        font-size: 14px;
        color: #989898;
        margin: auto
    }

    .chat_img {
        float: left;
        width: 11%
    }

    .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%
    }

    .chat_people {
        overflow: hidden;
        clear: both
    }

    .chat_list {
        border-bottom: 1px solid #c4c4c4;
        margin: 0;
        padding: 18px 16px 10px
    }

    .inbox_chat {
        height: 550px;
        overflow-y: scroll
    }

    .active_chat {
        background: #ebebeb
    }

    .incoming_msg_img {
        display: inline-block;
        width: 12%
    }

    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 77%
    }

    .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%
    }

    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0;
        color: #fff;
        padding: 5px 10px 5px 12px;
        width: 100%
    }

    .outgoing_msg {
        overflow: hidden;
        margin: 26px 0 26px
    }

    .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%
    }

    .type_msg {
        border-top: 1px solid #c4c4c4;
        position: relative
    }

    .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border: medium none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        height: 33px;
        position: absolute;
        right: 0;
        top: 11px;
        width: 33px
    }

    .messaging {
        padding: 0 0 50px 0
    }

    .msg_history {
        height: 300px;
        overflow-y: auto
    }

    .open > .dropdown-menu {
        display: block;
        width: 100%
    }

</style>
</body>
</html>

